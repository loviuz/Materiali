# Italian Linux Society - materiali

Raccolta di materiali per l'attività associativa Italian Linux Society.

## Images

### Logo Tux Clean

Logo tux, in SVG:

![logo tux](images/logo/italian_linux_society_penguin_circle_logo.svg)

Logo tux, in SVG and PNG:

https://commons.wikimedia.org/wiki/File:Linux_tux_circle_logo.svg

Logo tux, credits:

> Logo tux by Italian Linux Society, Roberto Guido, restyled by Virginia Foti, CC0.

### Logo Tux Loving Linux.it

Logo tux love Linux.it, in SVG:

![logo tux loving linux.it](images/promo/tux_love_linux_it.svg)

Printing tips:

- circle pins 1.5"
- circle stickers 2″
- circle stickers 3″

Logo tux, credits:

> Logo tux by Italian Linux Society, Roberto Guido, restyled by Virginia Foti, emojii+text by Valerio Bozzolan, CC0.

### Logo Italian Linux Society

Logo Italian Linux Society, in SVG:

![logo ILS](images/logo/italian_linux_society_penguin_circle_logo_complete.svg)

Logo Italian Linux Society, in SVG and PNG:

https://commons.wikimedia.org/wiki/File:Italian_Linux_Society_logo_text_penguin_circle_black.svg

Logo Italian Linux Society, credits:

> Logo Italian Linux Society by Virginia Foti, 2022. Penguin originally designed by Roberto Guido. CC0.

### Logo Usa Analizza Modifica Condividi

Logo Usa Analizza Modifica Condividi, SVG:

![usa analizza modifica condividi](images/promo/usa_analizza_modifica_condividi.svg)

Print tips:

```
Adesivi per interni
rotondo 4,5 cm
80 g carta adesiva bianca
```

Logo Usa Analizza Modifica Condividi, credits:

> Logo Usa Analizza Modifica Condividi, by Italian Linux Society, Roberto Guido. CC0.

### Logo Linux.it traditional

Logo Linux.it traditional, JPG:

![Linux.it](images/promo/linux_it_traditional.jpg)

Print tips:

```
Adesivi per interni
rotondo 3 cm
80 g carta adesiva bianca
```

Logo Linux.it credits:

> Logo Linux.it, Italian Linux Society, Roberto Guido. CC0

### Logo LinuxSi.com

![logo LinuxSi.com](images/promo/linuxsi.jpg)

> Logo LinuxSi.com, Italian Linux Society, Roberto Guido. CC0

Print tips:

```
Adesivi per esterni
rotondo 7 cm
90 µm pellicola adesiva bianca (senza intaglio sul retro)
```

### Brochure Italian Linux Society

Brochure Italian Linux Society, SVG (to be opened in Inkscape to see texts):

![brochure Italian Linux Society](images/promo/brochure_ils.svg)

Brochure Italian Linux Society, credits:

> Brochure Italian Linux Society by Roberto Guido. CC0.

Print tips:

```
Pieghevoli, piega a portafoglio
DIN lungo (9,8 x 21 cm)
135 g carta patinata opaca PEFC™
```
## Slide per Presentazioni

Per una presentazione con LibreOffice, ecco alcune (brevi) slide:

https://www.ils.org/sostieni/slides/

## Printables

### Crosswords Italian Linux Society v1

Crosswords A4 printable in B/W, in PDF and ODG format:

![crosswords Italian Linux Society v1](manifests/a4_crosswords_ils_1.pdf)

[crosswords Italian Linux Society v1](manifests/a4_crosswords_ils_1.odg)

Crosswords A4 printable in B/W, credits:

> Valerio Bozzolan, CC0.

## Licenza

Salvo ove diversamente indicato, tutti i contributi in questo repository (sia testo che immagini o altri materiali multimediali) sono pubblicati in pubblico dominio (Creative Commons Zero).

https://creativecommons.org/publicdomain/zero/1.0/
