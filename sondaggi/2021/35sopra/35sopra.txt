*************************
Come meglio descriveresti il tuo rapporto con Italian Linux Society?sono un socio dell'associazione                                                69
sto valutando se diventare socio dell'associazione                             31
non mi interessa diventare socio dell'associazione                             17
NaN                                                                            13
sono un socio impegnato in particolari attività per conto dell'associazione     5
Name: Come meglio descriveresti il tuo rapporto con Italian Linux Society?, dtype: int64*************************
Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Studio scolastico]1.0    65
NaN    49
3.0     8
2.0     6
4.0     4
5.0     3
Name: Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Studio scolastico], dtype: int64*************************
Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Formazione autonoma]3.0    42
4.0    30
5.0    29
2.0    25
1.0     5
NaN     4
Name: Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Formazione autonoma], dtype: int64*************************
Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Ricerca di lavoro]1.0    63
NaN    37
2.0    13
3.0     9
5.0     7
4.0     6
Name: Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Ricerca di lavoro], dtype: int64*************************
Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Lavoro]5.0    65
4.0    35
1.0    13
NaN     9
3.0     7
2.0     6
Name: Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Lavoro], dtype: int64*************************
Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Attivismo e hobby]2.0    40
3.0    34
4.0    24
5.0    19
1.0    14
NaN     4
Name: Ultimamente quanto tempo investi su ognuno dei seguenti temi? [Attivismo e hobby], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Sistemistica (sysadmin)]Sì    75
No    60
Name: Quali di questi temi ti coinvolgono particolarmente? [Sistemistica (sysadmin)], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Sviluppo software]No    90
Sì    45
Name: Quali di questi temi ti coinvolgono particolarmente? [Sviluppo software], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Consulenze e assistenza (software)]No    83
Sì    52
Name: Quali di questi temi ti coinvolgono particolarmente? [Consulenze e assistenza (software)], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Riparazioni / assemblaggio (hardware)]No    102
Sì     33
Name: Quali di questi temi ti coinvolgono particolarmente? [Riparazioni / assemblaggio (hardware)], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Sistemi integrati (embedded)]No    113
Sì     22
Name: Quali di questi temi ti coinvolgono particolarmente? [Sistemi integrati (embedded)], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Machine learning]No    119
Sì     16
Name: Quali di questi temi ti coinvolgono particolarmente? [Machine learning], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Istruzione e Formazione]No    70
Sì    65
Name: Quali di questi temi ti coinvolgono particolarmente? [Istruzione e Formazione], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Gestione personale o progetti]No    92
Sì    43
Name: Quali di questi temi ti coinvolgono particolarmente? [Gestione personale o progetti], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Eventi]No    100
Sì     35
Name: Quali di questi temi ti coinvolgono particolarmente? [Eventi], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Comunicazione]No    108
Sì     27
Name: Quali di questi temi ti coinvolgono particolarmente? [Comunicazione], dtype: int64*************************
Quali di questi temi ti coinvolgono particolarmente? [Vendite]No    130
Sì      5
Name: Quali di questi temi ti coinvolgono particolarmente? [Vendite], dtype: int64*************************
Ultimamente quanto tempo investi in questi contesti? [contributi diretti a software libero (sviluppi, segnalazione bug, donazioni, …)]1.0    60
NaN    22
3.0    20
2.0    18
4.0    10
5.0     5
Name: Ultimamente quanto tempo investi in questi contesti? [contributi diretti a software libero (sviluppi, segnalazione bug, donazioni, …)], dtype: int64*************************
Ultimamente quanto tempo investi in questi contesti? [contributi indiretti a software libero (utilizzo, promozione, discussioni, attività, …)]2.0    35
3.0    33
4.0    21
1.0    19
5.0    14
NaN    13
Name: Ultimamente quanto tempo investi in questi contesti? [contributi indiretti a software libero (utilizzo, promozione, discussioni, attività, …)], dtype: int64*************************
Ultimamente quanto tempo investi in questi contesti? [contributi ai progetti Wikimedia (Wikipedia, …)]1.0    58
NaN    37
2.0    22
3.0    13
4.0     3
5.0     2
Name: Ultimamente quanto tempo investi in questi contesti? [contributi ai progetti Wikimedia (Wikipedia, …)], dtype: int64*************************
Ultimamente quanto tempo investi in questi contesti? [contributi a OpenStreetMap]1.0    75
NaN    40
2.0    12
3.0     6
4.0     1
5.0     1
Name: Ultimamente quanto tempo investi in questi contesti? [contributi a OpenStreetMap], dtype: int64*************************
Ultimamente quanto tempo investi in questi contesti? [altro volontariato]1.0    38
NaN    27
2.0    24
3.0    22
4.0    16
5.0     8
Name: Ultimamente quanto tempo investi in questi contesti? [altro volontariato], dtype: int64*************************
Come valuti l'adozione al software libero intorno a te? [Software libero nella vita privata]5.0    58
3.0    24
2.0    22
4.0    17
1.0    12
NaN     2
Name: Come valuti l'adozione al software libero intorno a te? [Software libero nella vita privata], dtype: int64*************************
Come valuti l'adozione al software libero intorno a te? [Software libero in famiglia]5.0    42
3.0    30
4.0    26
2.0    22
1.0    12
NaN     3
Name: Come valuti l'adozione al software libero intorno a te? [Software libero in famiglia], dtype: int64*************************
Come valuti l'adozione al software libero intorno a te? [Software libero a scuola]1.0    48
2.0    30
NaN    23
5.0    13
3.0    11
4.0    10
Name: Come valuti l'adozione al software libero intorno a te? [Software libero a scuola], dtype: int64*************************
Come valuti l'adozione al software libero intorno a te? [Software libero a lavoro]1.0    35
2.0    27
4.0    25
3.0    23
5.0    20
NaN     5
Name: Come valuti l'adozione al software libero intorno a te? [Software libero a lavoro], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [siti web (linux.it · ils.org)]NaN    45
3.0    36
4.0    28
5.0    10
1.0     9
2.0     7
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [siti web (linux.it · ils.org)], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [spedizione adesivi]NaN    75
3.0    17
4.0    14
5.0    12
1.0    10
2.0     7
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [spedizione adesivi], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [mailing list]NaN    46
3.0    29
4.0    25
5.0    22
1.0     7
2.0     6
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [mailing list], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [forum.linux.it]NaN    58
4.0    22
3.0    22
1.0    13
2.0    12
5.0     8
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [forum.linux.it], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [planet.linux.it (raccolta di blog)]NaN    76
3.0    19
1.0    14
4.0    13
2.0     9
5.0     4
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [planet.linux.it (raccolta di blog)], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [servizi.linux.it (applicazioni per i soci)]NaN    76
3.0    21
4.0    15
1.0    13
5.0     6
2.0     4
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [servizi.linux.it (applicazioni per i soci)], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [lugmap.linux.it (mappa dei gruppi locali)]NaN    64
3.0    28
4.0    19
5.0    12
1.0     7
2.0     5
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [lugmap.linux.it (mappa dei gruppi locali)], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [posta personale @linux.it]NaN    66
5.0    23
3.0    22
4.0    10
1.0     8
2.0     6
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [posta personale @linux.it], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [assistenza tecnica nazionale]NaN    91
3.0    17
1.0    10
2.0     9
4.0     4
5.0     4
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [assistenza tecnica nazionale], dtype: int64*************************
Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [eventi.merge-it.net (servizio videocall)]NaN    84
3.0    17
1.0    11
4.0     9
5.0     8
2.0     6
Name: Allo stato attuale, come valuti complessivamente questi servizi Italian Linux Society? [eventi.merge-it.net (servizio videocall)], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [mailing list]5.0    40
3.0    30
NaN    25
4.0    24
2.0    11
1.0     5
Name: In ogni caso, quanto ritieni strategici quei servizi? [mailing list], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [spedizione adesivi]NaN    38
3.0    29
1.0    25
2.0    17
4.0    15
5.0    11
Name: In ogni caso, quanto ritieni strategici quei servizi? [spedizione adesivi], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [siti web (linux.it · ils.org)]5.0    58
NaN    27
4.0    24
3.0    20
2.0     4
1.0     2
Name: In ogni caso, quanto ritieni strategici quei servizi? [siti web (linux.it · ils.org)], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [forum.linux.it]NaN    37
4.0    30
5.0    28
3.0    23
1.0    11
2.0     6
Name: In ogni caso, quanto ritieni strategici quei servizi? [forum.linux.it], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [planet.linux.it (raccolta di blog)]NaN    48
4.0    25
3.0    20
5.0    19
2.0    12
1.0    11
Name: In ogni caso, quanto ritieni strategici quei servizi? [planet.linux.it (raccolta di blog)], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [servizi.linux.it (applicazioni per i soci)]NaN    43
5.0    30
3.0    27
4.0    25
2.0     5
1.0     5
Name: In ogni caso, quanto ritieni strategici quei servizi? [servizi.linux.it (applicazioni per i soci)], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [LUG Map (mappa dei gruppi locali)]5.0    45
NaN    31
3.0    28
4.0    26
2.0     4
1.0     1
Name: In ogni caso, quanto ritieni strategici quei servizi? [LUG Map (mappa dei gruppi locali)], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [posta personale @linux.it]NaN    41
5.0    36
3.0    25
2.0    15
1.0     9
4.0     9
Name: In ogni caso, quanto ritieni strategici quei servizi? [posta personale @linux.it], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [assistenza tecnica nazionale]NaN    48
5.0    26
4.0    22
3.0    22
1.0     9
2.0     8
Name: In ogni caso, quanto ritieni strategici quei servizi? [assistenza tecnica nazionale], dtype: int64*************************
In ogni caso, quanto ritieni strategici quei servizi? [eventi.merge-it.net (servizio di videocall)]NaN    56
5.0    25
3.0    23
4.0    19
1.0     7
2.0     5
Name: In ogni caso, quanto ritieni strategici quei servizi? [eventi.merge-it.net (servizio di videocall)], dtype: int64*************************
Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 1]migliorare la comunicazione                36
favorire interscambio fra gruppi locali    23
NaN                                        22
favorire partecipazione dei soci           21
investire nei progetti                     17
migliorare i servizi ai soci               16
Name: Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 1], dtype: int64*************************
Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 2]favorire interscambio fra gruppi locali    34
NaN                                        27
favorire partecipazione dei soci           26
migliorare la comunicazione                21
investire nei progetti                     16
migliorare i servizi ai soci               11
Name: Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 2], dtype: int64*************************
Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 3]NaN                                        63
favorire interscambio fra gruppi locali    17
favorire partecipazione dei soci           16
migliorare la comunicazione                16
investire nei progetti                     15
migliorare i servizi ai soci                8
Name: Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 3], dtype: int64*************************
Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 4]NaN                                        82
migliorare i servizi ai soci               16
investire nei progetti                     16
favorire partecipazione dei soci            7
favorire interscambio fra gruppi locali     7
migliorare la comunicazione                 7
Name: Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 4], dtype: int64*************************
Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 5]NaN                                        86
migliorare i servizi ai soci               16
investire nei progetti                     11
favorire partecipazione dei soci           10
migliorare la comunicazione                 7
favorire interscambio fra gruppi locali     5
Name: Prenditi un momento per selezionare le tue priorità per Italian Linux Society. [Classifica 5], dtype: int64*************************
In quale provincia risiedi maggiormente?BS     14
PN     12
MI     11
TO      9
TV      8
PG      6
MN      6
CO      5
FI      5
GE      5
NaN     4
FG      3
VE      2
PE      2
RI      2
VA      2
AP      2
LU      2
RE      2
PA      2
RM      2
BL      2
CA      2
BA      2
UD      2
EE      1
PU      1
TN      1
CE      1
PT      1
LC      1
NO      1
IM      1
GR      1
SC      1
PO      1
CS      1
PV      1
LI      1
SI      1
MS      1
BO      1
MO      1
RN      1
LT      1
AL      1
Name: In quale provincia risiedi maggiormente?, dtype: int64*************************
Quanti anni hai?47-52    39
41-46    33
35-40    20
53-58    16
59-64    14
71-76     6
65-70     5
77+       2
Name: Quanti anni hai?, dtype: int64